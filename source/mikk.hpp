#pragma once

#include <vector>

#include "glm/glm.hpp"

struct Vertex {
    glm::vec3 Position;
    glm::vec3 Normal;
    glm::vec3 Tangent;
    glm::vec2 Uv;
    float Sign;
};

typedef std::pair<std::vector<Vertex>, std::vector<int>> VertexTable;

class MikkContext {
public:
    void addVertex(glm::vec3 co, glm::vec3 normal, glm::vec2 uv);
    void calculate();
    void setTangent(int index, glm::vec3 tangent, float sign);
    glm::vec3 getPosition(int index) const;
    glm::vec3 getNormal(int index) const;
    glm::vec3 getTangent(int index) const;
    float getSign(int index) const;
    glm::vec2 getUv(int index) const;
    int getSize() const;
    VertexTable computeVertexTable() const;
private:
    std::vector<Vertex> Vertices{};
};