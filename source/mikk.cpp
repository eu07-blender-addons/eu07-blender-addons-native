
#include <cstdio>

#include "mikk.hpp"
#include "SpatialSort.hpp"

#include "mikktspace.h"
#include "glm/gtc/epsilon.hpp"

using namespace glm;


  namespace {

    int getNumFaces(const SMikkTSpaceContext* context) {
      const MikkContext* userData = (MikkContext*)context->m_pUserData;
      return userData->getSize() / 3;
    }

    int getNumVerticesOfFace(const SMikkTSpaceContext* context, const int iFace) {
      return 3;
    }

    void getPosition(const SMikkTSpaceContext* context, float* vertex, const int iFace, const int iVert) {
      MikkContext* userData = (MikkContext*)context->m_pUserData;
      *(vec3*)vertex = userData->getPosition(iFace * 3 + iVert);
    }

    void getNormal(const SMikkTSpaceContext* context, float* normal, const int iFace, const int iVert) {
      MikkContext* userData = (MikkContext*)context->m_pUserData;
      *(vec3*)normal = userData->getNormal(iFace * 3 + iVert);
    }

    void getTexCoord(const SMikkTSpaceContext* context, float* uv, const int iFace, const int iVert) {
      MikkContext* userData = (MikkContext*)context->m_pUserData;
      *(vec2*)uv = userData->getUv(iFace * 3 + iVert);
    }

    void setTangentSpace(const SMikkTSpaceContext* context, const float* tangent, const float sign, const int iFace, const int iVert) {
      MikkContext* userData = (MikkContext*)context->m_pUserData;
      userData->setTangent(iFace * 3 + iVert, *(vec3*)tangent, sign);
    }

  }

void MikkContext::addVertex(vec3 co, vec3 normal, vec2 uv) {
    auto& vertex = Vertices.emplace_back();
    vertex.Position = co;
    vertex.Normal = normal;
    vertex.Uv = uv;
}

void MikkContext::calculate() {
    // MTS callback interface
    SMikkTSpaceInterface iMikkTS{};
    iMikkTS.m_getNumFaces = &::getNumFaces;
    iMikkTS.m_getNumVerticesOfFace = &::getNumVerticesOfFace;
    iMikkTS.m_getPosition = &::getPosition;
    iMikkTS.m_getNormal = &::getNormal;
    iMikkTS.m_getTexCoord = &::getTexCoord;
    iMikkTS.m_setTSpaceBasic = &::setTangentSpace;

    // MTS context
    SMikkTSpaceContext context{};
    context.m_pUserData = this;
    context.m_pInterface = &iMikkTS;

    // Create tangent space
    genTangSpaceDefault(&context);
}
    
void MikkContext::setTangent(int index, vec3 tangent, float sign) {
    auto& vertex = Vertices[index];
    vertex.Tangent = tangent;
    vertex.Sign = sign;
}
    
vec3 MikkContext::getPosition(int index) const {
    return Vertices[index].Position;
}
    
vec3 MikkContext::getNormal(int index) const {
    return Vertices[index].Normal;
}
    
vec3 MikkContext::getTangent(int index) const {
    return Vertices[index].Tangent;
}
    
float MikkContext::getSign(int index) const {
    return Vertices[index].Sign;
}
    
vec2 MikkContext::getUv(int index) const {
    return Vertices[index].Uv;
}

int MikkContext::getSize() const {
    return (int)Vertices.size();
}

VertexTable MikkContext::computeVertexTable() const {
  VertexTable result{};

  auto& outVertices = std::get<0>(result);
  auto& outIndices = std::get<1>(result);

  // Initialize spatial sort
  SpatialSort sort((const char*)Vertices.data(), Vertices.size(), sizeof(Vertex), offsetof(Vertex, Position));

  // Allocate index buffer
  outIndices.resize(Vertices.size(), -1);
  for (int i = 0; i < outIndices.size(); ++i) {
    // Skip if index already assigned
    if (outIndices[i] >= 0) continue;

    // Create new vertex
    outIndices[i] = (int)outVertices.size();
    const auto& vertex = outVertices.emplace_back(Vertices[i]);

    // Find and optimize away identical vertices
    sort.FindPositions(vertex.Position, 1e-5f, [&](const int index) -> bool {
      if ((index == i) || outIndices[index] >= 0) return false;
      const auto& rhs = Vertices[index];
      if (
        (vertex.Sign != rhs.Sign) ||
        (glm::dot(vertex.Normal, rhs.Normal) < 0.99984769515f) || // = difference is not greater than 1deg
        (glm::dot(vertex.Tangent, rhs.Tangent) < 0.99984769515f) ||
        glm::any(glm::epsilonNotEqual(vertex.Uv, rhs.Uv, 1e-6f))) return false;
      outIndices[index] = outIndices[i];
      return false;
      });
  }
  return result;
}