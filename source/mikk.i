

%include "windows.i"

%module mikktspace
%{
/* Includes the header in the wrapper code */
#include "mikk.hpp"
%}

%typemap(in) glm::vec2 {
    $1 = glm::vec2{};
    for(int i = 0; i < 2; ++i)
        $1[i] = (float)PyFloat_AsDouble(PyTuple_GetItem($input, i));
}
%typemap(out) glm::vec2 {
    $result = PyTuple_New(2);
    for(int i = 0; i < 2; ++i)
        PyTuple_SetItem($result, i, PyFloat_FromDouble($1[i]));
}

%typemap(in) glm::vec3 {
    $1 = glm::vec3{};
    for(int i = 0; i < 3; ++i)
        $1[i] = (float)PyFloat_AsDouble(PyTuple_GetItem($input, i));
}
%typemap(out) glm::vec3 {
    $result = PyTuple_New(3);
    for(int i = 0; i < 3; ++i)
        PyTuple_SetItem($result, i, PyFloat_FromDouble($1[i]));
}

%typemap(out) Vertex {
    $result = PyTuple_New(12);
    for(int i = 0; i < 3; ++i)
        PyTuple_SetItem($result, i + 0, PyFloat_FromDouble($1.Position[i]));
    for(int i = 0; i < 3; ++i)
        PyTuple_SetItem($result, i + 3, PyFloat_FromDouble($1.Normal[i]));
    for(int i = 0; i < 2; ++i)
        PyTuple_SetItem($result, i + 6, PyFloat_FromDouble($1.Uv[i]));
    for(int i = 0; i < 3; ++i)
        PyTuple_SetItem($result, i + 8, PyFloat_FromDouble($1.Tangent[i]));
    PyTuple_SetItem($result, i + 11, PyFloat_FromDouble($1.Sign));
}

%typemap(out) VertexTable {
    $result = PyTuple_New(2);
    const auto& verts = std::get<0>($1);
    const auto& inds = std::get<1>($1);
    auto vertList = PyList_New(verts.size());
    auto indList = PyList_New(inds.size());
    PyTuple_SetItem($result, 0, vertList);
    PyTuple_SetItem($result, 1, indList);
    for(int vi = 0; vi < verts.size(); ++vi) {
        const auto& v = verts[vi];
        auto vTuple = PyTuple_New(12);
        PyList_SetItem(vertList, vi, vTuple);
        for(int i = 0; i < 3; ++i)
            PyTuple_SetItem(vTuple, i, PyFloat_FromDouble(v.Position[i]));
        for(int i = 0; i < 3; ++i)
            PyTuple_SetItem(vTuple, 3 + i, PyFloat_FromDouble(v.Normal[i]));
        for(int i = 0; i < 2; ++i)
            PyTuple_SetItem(vTuple, 6 + i, PyFloat_FromDouble(v.Uv[i]));
        for(int i = 0; i < 3; ++i)
            PyTuple_SetItem(vTuple, 8 + i, PyFloat_FromDouble(v.Tangent[i]));
        PyTuple_SetItem(vTuple, 11, PyFloat_FromDouble(v.Sign));
    }
    for(int ii = 0; ii < inds.size(); ++ii) {
        PyList_SetItem(indList, ii, PyInt_FromLong(inds[ii]));
    }
}

/* Parse the header file to generate wrappers */
%include "mikk.hpp"